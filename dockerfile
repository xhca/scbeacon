FROM rocker/verse:3.6.0

# required
MAINTAINER Hongxu Gong <hd2326@columbia.edu >

# copy the repo contents into the docker image at `/scBeacon`
COPY . /scBeacon

# install the dependencies of the R package located at `/scBeacon`
RUN    apt-get -y update -qq                      \
    && apt-get install -y --no-install-recommends \
           libgsl0-dev                            \
    && R -e "install.packages('viper', repos='http://cran.rstudio.com/')"   \
    && R -e "install.packages('Matrix', repos='http://cran.rstudio.com/')"  \
    && R -e "install.packages('igraph', repos='http://cran.rstudio.com/')"  \
    && R -e "install.packages('biomaRt', repos='http://cran.rstudio.com/')" \
    && R -e "install.packages('Rtsne', repos='http://cran.rstudio.com/')" 
