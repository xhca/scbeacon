#' Generate Centroids
#'
#' Centroids manipulation function.
#' Generate centroids from expression matrices.
#'
#' @param files (character) Links to _matrix.mtx files containing single gene-cell expression matrix.
#' Corresponding _genes.tsv and _cells.tsv files of .mtx files should also be given.
#' Names of .mtx file should follow "compulsory entries+optional entires" metadata format.
#' Compulsory/optional regions are separated by "+", while entries are separated by "_".
#' Compulsory entries are recorded as "Lab_Platform_Species_Developmental-Stage_Batch_Tissue_Cell-Cluster".
#' Optional entries are flexible, e.g. "Genetic-Background_Treatment".
#'
#' @param species (character) Species. All given expression profiles should from same species.
#'
#' @return centroids in seperate *centroid.rda files.
#'
#' @author DING, HONGXU (hd2326@columbia.edu)
#'
#' @importFrom Matrix as.matrix
#' @importFrom Matrix readMM
#'
#' @export

generate_centroids <- function (files, species = "hsapiens"){
    table <- gene_id_converter(species)
    table <- table[!duplicated(table$external_gene_name), ]

    for (f in files) {
        message(f)

        exp   <- as.matrix(readMM(f))
        genes <- read.delim(gsub("matrix.mtx", "genes.tsv", f), header=F, stringsAsFactors=F)
        cells <- read.delim(gsub("matrix.mtx", "cells.tsv", f), header=F, stringsAsFactors=F)

        rownames(exp) <- genes$V1
        colnames(exp) <- cells$V1

        id            <- which.max(apply(table, 2, function(x, y) sum(x %in% y), y=rownames(exp)))
        exp           <- exp[rownames(exp) %in% table[, id], ]
        rownames(exp) <- table$external_gene_name[match(rownames(exp), table[, id])]

        centroid <- structure(
            matrix(0, nrow(table), ncol(exp)),
            dimnames=list(table$external_gene_name, colnames(exp))
        )
        centroid[rownames(exp), ] <- exp

        centroid <- apply(centroid, 2, rank)/nrow(centroid)

        metadata <- sapply(strsplit(f       , split="_"  ), function(x) paste(x[1:(length(x)-1)], collapse="_"))
        metadata <- sapply(strsplit(metadata, split="/"  ), function(x) x[length(x)]                           )
        metadata <- sapply(strsplit(metadata, split="[+]"), function(x) x[1]                                   )

        centroid <- matrix(rowMeans(centroid), nrow(centroid), 1, dimnames = list(rownames(centroid), metadata))

        write.table(
            centroid,
            file=paste(metadata, "_centroid.tsv", sep = ""),
            quote=F
        )
    }
}
