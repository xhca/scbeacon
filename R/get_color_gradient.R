#' Generate Color Gradient
#'
#' Basic biology-interpreting function.
#' Generate color gradient for, e.g. gene expression.
#'
#' @param x (numeric) Vector based on which color gradient is generated.
#'
#' @return (character) Colors.
#'
#' @author DING, HONGXU (hd2326@columbia.edu)
#'
#' @export

get_color_gradient <- function(x, col=rainbow(100), breaks=seq(-2, 2, length.out=100)) {
    if (length(col) == length(breaks)) {
        col <- col[unlist(lapply(x, function(xx, breaks) which.min(abs(xx-breaks)), breaks=breaks))]
    }

    else {
        col <- NULL
        message("error, vector col and breaks should be the same length")
    }

    return(col)
}
