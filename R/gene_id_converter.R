#' Gene ID Converter
#'
#' Basic biology-interpreting function.
#' Correspondence table among gene name, entrez ID and ensembl ID.
#'
#' @param species (character) Species.
#'
#' @return (character matrix) Correspondence table.
#'
#' @author DING, HONGXU (hd2326@columbia.edu)
#'
#' @importFrom biomaRt useMart
#' @importFrom biomaRt useDataset
#' @importFrom biomaRt getBM
#'
#' @export

gene_id_converter <- function(species="hsapiens") {
    mart  <- useDataset(paste(species, "_gene_ensembl", sep = ""), useMart("ensembl"))
    table <- getBM(attributes=c("ensembl_gene_id", "entrezgene_id", "external_gene_name"), mart=mart)
    table <- table[apply(table, 1, function(x) sum(is.na(x)) == 0), ]

    return(table)
}
