# scBeacon

From repo: https://github.com/hd2326/BiologicalProcessActivity
Used in paper: https://www.nature.com/articles/s41467-019-12924-w

This repository is for me to play around with scBeacon (R package for single-cell analysis) in a
way that I can run it against an object-based storage system.

Initial benchmarking will be against a local file system, and then against a postgres database
(local first, then remote). With these as references, the progression towards an object-based
storage system backed by computational storage devices should become a fairly straightforward
narrative.
